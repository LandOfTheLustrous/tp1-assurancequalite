package diningtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runners.model.TestTimedOutException;

import dinningcomponents.Chopstick;
import dinningcomponents.Philosopher;
import dinninggraphics.ColorSelector;
import dinninggraphics.GraphicChopstick;
import dinninggraphics.GraphicPlate;
import dinninggraphics.GraphicTable;
import dinningservices.DiningSetup;

public class Testes {
	
	private DiningSetup dining = null;
	private Philosopher philosopher1 = null;
	private Philosopher philosopher2 = null;
	private Chopstick chopstick1 = null;
	private Chopstick chopstick2 = null;
	private Chopstick chopstick3 = null;
	private Chopstick chopstick4 = null;
	private GraphicPlate[] plates = null;
	private GraphicChopstick[] sticks = null;
	private GraphicTable table = null;
	
	@Rule
	public Timeout globalTimeout = Timeout.millis(200);
	
	@After
	public void test() {
		dining = null;
		philosopher1 = null;
		philosopher2 = null;
		chopstick1 = null;
		chopstick2 = null;
		chopstick3 = null;
		chopstick4 = null;
		plates = null;
		sticks = null;
		table = null;
	}
	
	@Test
	public void createDataTest() {
		dining = new DiningSetup();
		
		assertEquals(5, dining.getPhilosophers().size());
		assertEquals(5, dining.getChopsticks().size());
	}
	
	@Test
	public void chopstickLibreTest() throws InterruptedException {
		dining = new DiningSetup();
		philosopher1 = new Philosopher(1, dining.getTable(), new Chopstick(1), new Chopstick(2));
		assertTrue(philosopher1.getLeft().isLibre() && philosopher1.getRight().isLibre());
		
		philosopher1.getLeft().take();
		philosopher1.getRight().take();
		
		assertTrue(!(philosopher1.getLeft().isLibre() && philosopher1.getRight().isLibre()));
		
		philosopher1.getLeft().release();
		philosopher1.getRight().release();
		
		assertTrue(philosopher1.getLeft().isLibre() && philosopher1.getRight().isLibre());
	}
	
	@Test
	public void goodColorTest() {
		dining = new DiningSetup();
		plates = dining.getTable().getPlates();
		sticks = dining.getTable().getChops();
		for (int i = 0; i < plates.length; i++) {
			assertTrue(plates[i].getColor() == ColorSelector.getRightColor(ColorSelector.BLACK));
			assertTrue(sticks[i].getColor() == ColorSelector.getRightColor(ColorSelector.BLACK));

			plates[i].setColor(i);
			assertTrue(plates[i].getColor() == ColorSelector.getRightColor(i));
		}
	}
	
	@Test
	public void isThinkingTest() {
		dining = new DiningSetup();
		table = dining.getTable();
		
		table.isThinking(0);
		plates = table.getPlates();
		
		assertTrue(plates[0].getColor() == ColorSelector.getRightColor(ColorSelector.BLACK));
	}
	
	@Test
	public void isHungryTest() {
		dining = new DiningSetup();
		table = dining.getTable();
		
		table.isHungry(0);
		plates = table.getPlates();
		
		assertTrue(plates[0].getColor() == ColorSelector.getRightColor(ColorSelector.RED));
	}
	
	@Test
	public void badColorTest() {
		assertTrue(ColorSelector.getRightColor(200) == null);
	}
	
	@Test
	public void modificationChopsticksTest() {
		chopstick1 = new Chopstick(1);
		chopstick2 = new Chopstick(2);
		
		philosopher1 = new Philosopher(1, null, chopstick1, chopstick2);
		
		assertEquals(chopstick1.getId(), philosopher1.getLeft().getId());
		assertEquals(chopstick2.getId(), philosopher1.getRight().getId());
		
		chopstick3 = new Chopstick(3);
		chopstick4 = new Chopstick(4);
		
		philosopher1.setLeft(chopstick3);
		philosopher1.setRight(chopstick4);
		
		assertEquals(chopstick3.getId(), philosopher1.getLeft().getId());
		assertEquals(chopstick4.getId(), philosopher1.getRight().getId());
	}
	
	public void takingChopsticksBlockTest() throws TestTimedOutException, InterruptedException {
		chopstick1 = new Chopstick(1);
		chopstick2 = new Chopstick(2);
		chopstick3 = new Chopstick(3);
		
		philosopher1 = new Philosopher(11, null, chopstick1, chopstick2);
		philosopher2 = new Philosopher(12, null, chopstick2, chopstick3);
		
		try {
			philosopher1.getLeft().take();
			philosopher1.getRight().take();
			
			philosopher2.getLeft().take();
			philosopher2.getRight().take();			
		} catch (Exception e) {
			assertTrue(true);
		}
		assertFalse(true);
	}
	
//	@Test
//	public void deadLockTest() {
		// Impossible � tester avec des tests JUnit avec la fa�on dont je l'ai fix.
		// Car si je cr�er un nouveau jeu qui commence avec un deadlock, le programme va rester en deadlock
		// mais niveau �x�cution, il ne le sera jamais (Peut tester en mettant les 3 vitesses � 1)
//	}

}
