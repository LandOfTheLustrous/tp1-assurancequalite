package dinningcomponents;

public class Chopstick {
	private int id;
	private boolean libre;

	public Chopstick(int id) {
		this.id = id;
		libre = true;
	}

	public synchronized void take() throws InterruptedException {
		while (!libre) {
			wait();
		}
		libre = false;
	}

	public synchronized void release() {
		notifyAll();
		libre = true;
	}

	public int getId() {
		return id;
	}

	public boolean isLibre() {
		return libre;
	}
	
	
}
