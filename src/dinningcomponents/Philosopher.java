package dinningcomponents;

import dinninggraphics.GraphicTable;

public class Philosopher extends Thread {
	
	private GraphicTable table;
	private Chopstick left;
	private Chopstick right;
	private int id;
	
	private final int TIME_TO_THINK_MAX = 5000; // 5000
	private final int TIME_NEXT_FORK_MAX = 100; // 100
	private final int TIME_TO_EAT_MAX = 2000; // 2000

	public Philosopher(int id, GraphicTable table, Chopstick left, Chopstick right) {
		this.id = id;
		this.table = table;
		this.left = left;
		this.right = right;
		setName("Philosopher " + id);
	}

	public void run() {
		while (true) {
			try {
				thinking();
				table.isHungry(id);
				takeChopsticks();
				releaseChopsticks();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void takeChopsticks() throws InterruptedException {
		takeLeft();
		takeRight();
	}

	private void takeLeft() throws InterruptedException {
		left.take();
		table.takeChopstick(id, left.getId());
		sleep(TIME_NEXT_FORK_MAX);
	}

	private void takeRight() throws InterruptedException {
		right.take();
		table.takeChopstick(id, right.getId());
		sleep((long) (Math.random() * TIME_TO_EAT_MAX));
	}

	private void releaseChopsticks() {
		releaseLeft();
		releaseRight();
	}

	private void releaseLeft() {
		table.releaseChopstick(id, left.getId());
		left.release();
	}

	private void releaseRight() {
		table.releaseChopstick(id, right.getId());
		right.release();
	}

	private void thinking() throws InterruptedException {
		table.isThinking(id);
		sleep((long) (Math.random() * TIME_TO_THINK_MAX));
	}

	public Chopstick getLeft() {
		return left;
	}
	

	public void setLeft(Chopstick left) {
		this.left = left;
	}

	public Chopstick getRight() {
		return right;
	}

	public void setRight(Chopstick right) {
		this.right = right;
	}


}
