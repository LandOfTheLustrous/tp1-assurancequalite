package dinninggraphics;
import java.awt.*;

public class GraphicChopstick {
	private static final int CHOPSTICK_LENGTH = 15;
	private Point coordStart;
	private Point coordEnd;
	private Color color;
	private int angle;
	private int philosopherId;

	GraphicChopstick(int id, Point center, Point coordStart, Point coordEnd) {
		this.philosopherId = ColorSelector.BLACK;
		setColor(philosopherId);

		this.angle = changeAngleDependingOnHowManyAreCreated(id);
		initialiseCoordinate(center, coordStart, coordEnd);
	}

	private int changeAngleDependingOnHowManyAreCreated(int id) {
		return 72 * id + 36;
	}

	private void initialiseCoordinate(Point center, Point coordStart, Point coordEnd) {
		this.coordStart = initialiseWindowCoordinate(center, coordStart);
		this.coordEnd = initialiseWindowCoordinate(center, coordEnd);
	}

	private Point initialiseWindowCoordinate(Point center, Point coordStart) {
		Point modifiedPoint = null;
		modifiedPoint = Graphic2D.rotate(coordStart, center, angle);
		modifiedPoint.y += CHOPSTICK_LENGTH;
		return modifiedPoint;
	}

	public void draw(Graphics g) {
		g.setColor(color);
		g.drawLine(coordStart.x, coordStart.y, coordEnd.x, coordEnd.y);
	}

	public void setColor(int phId) {
		this.philosopherId = phId;
		this.color = ColorSelector.getRightColor(phId);
	}

	public Color getColor() {
		return color;
	}


}
