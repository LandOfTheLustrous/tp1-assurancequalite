package dinninggraphics;
import java.awt.Graphics;
import java.awt.Point;

public class GraphicTable extends WindowSettings {
	
	private static final long serialVersionUID = 1L;
	
	private static final int PLATE_SIZE = 20;
	private static final int NUMBER_OF_PHILOSOPHER = 5;
	
	private static final Point CENTER_POINT = new Point(WIDTH / 2, HEIGHT / 2);
	private static final Point STARTING_POINT = new Point(CENTER_POINT.x, CENTER_POINT.y - 70);
	private static final Point ENDING_POINT = new Point(CENTER_POINT.x, CENTER_POINT.y - 40);
	
	private GraphicPlate plates[];
	private GraphicChopstick chops[];

	public GraphicTable() {
		super();
		drawComponents();
	}
	
	private void drawComponents() {
		drawAllPlates();
		drawAllChopsticks();
	}

	private void drawAllChopsticks() {
		chops = new GraphicChopstick[NUMBER_OF_PHILOSOPHER];
		for (int i = 0; i < NUMBER_OF_PHILOSOPHER; i++) {
			chops[i] = makeChopstick(i);
		}
	}

	private void drawAllPlates() {
		plates = new GraphicPlate[NUMBER_OF_PHILOSOPHER];
		for (int i = 0; i < NUMBER_OF_PHILOSOPHER; i++) {
			plates[i] = makePlate(i);
		}
	}

	private GraphicPlate makePlate(int i) {
		return new GraphicPlate(i, CENTER_POINT, STARTING_POINT, PLATE_SIZE);
	}

	private GraphicChopstick makeChopstick(int i) {
		return new GraphicChopstick(i, CENTER_POINT, STARTING_POINT, ENDING_POINT);
	}
	
	public void isHungry(int phID) {
		plates[phID].setColor(phID);
		repaint();
	}

	public void isThinking(int phID) {
		plates[phID].setColor(ColorSelector.BLACK);
		repaint();
	}

	public void takeChopstick(int phID, int chID) {
		chops[chID].setColor(phID);
		repaint();
	}

	public void releaseChopstick(int phID, int chID) {
		chops[chID].setColor(ColorSelector.BLACK);
		repaint();
	}

	public void paint(Graphics g) {
		for (int i = 0; i < NUMBER_OF_PHILOSOPHER; i++) {
			plates[i].draw(g);
			chops[i].draw(g);
		}
	}

	public GraphicPlate[] getPlates() {
		return plates;
	}

	public GraphicChopstick[] getChops() {
		return chops;
	}

	
	

}
