package dinninggraphics;

import java.awt.Color;

public class ColorSelector {
	
	public static final int BLACK = -1;
	public static final int RED = 0;
	public static final int BLUE = 1;
	public static final int GREEN = 2;
	public static final int YELLOW = 3;
	public static final int WHITE = 4;

	public static Color getRightColor(int phiId) {

		switch (phiId) {
		case BLACK:
			return Color.black;
		case RED:
			return Color.red;
		case BLUE:
			return Color.blue;
		case GREEN:
			return Color.green;
		case YELLOW:
			return Color.yellow;
		case WHITE:
			return Color.white;
		}
		return null;
	}
}
