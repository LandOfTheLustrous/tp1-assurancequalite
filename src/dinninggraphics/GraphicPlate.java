package dinninggraphics;
import java.awt.*;

public class GraphicPlate {
	private static final int CIRCLE_FRACTION = 72;
	private Point coords;
	private Color color;
	private int phId;
	private int angle;
	private int size;

	GraphicPlate(int ident, Point center, Point coords, int size) {
		this.size = size;
		this.phId = ColorSelector.BLACK;
		setColor(phId);

		this.angle = CIRCLE_FRACTION * ident;
		this.coords = new Point(Graphic2D.rotate(coords, center, angle));
		this.coords.x -= 10;
		this.coords.y += 5;
	}

	public void setColor(int phId) {
		this.phId = phId;
		this.color = ColorSelector.getRightColor(phId);
	}

	public void draw(Graphics g) {
		g.setColor(color);
		g.fillOval(coords.x, coords.y, size, size);
	}

	public Color getColor() {
		return color;
	}
}
