package dinninggraphics;

import java.awt.*;

public class Graphic2D {

	public static Point rotate(Point start, Point center, int angle) {
		double rotateBasedOnAngle = Math.PI / (180.0 / angle);

		return calculatePoint(start, center, rotateBasedOnAngle);
	}

	private static Point calculatePoint(Point start, Point center, double rotateBasedOnAngle) {
		return new Point(makeXCoords(start, center, rotateBasedOnAngle), makeYCoords(start, center, rotateBasedOnAngle));
	}

	private static int makeYCoords(Point start, Point center, double rotateBasedOnAngle) {
		return (int) (start.x * Math.sin(rotateBasedOnAngle) + start.y * Math.cos(rotateBasedOnAngle)
				- Math.sin(rotateBasedOnAngle) * center.x - Math.cos(rotateBasedOnAngle) * center.y + center.y);
	}

	private static int makeXCoords(Point start, Point center, double rotateBasedOnAngle) {
		return (int) (start.x * Math.cos(rotateBasedOnAngle) - start.y * Math.sin(rotateBasedOnAngle)
				- Math.cos(rotateBasedOnAngle) * center.x + Math.sin(rotateBasedOnAngle) * center.y + center.x);
	}

}
