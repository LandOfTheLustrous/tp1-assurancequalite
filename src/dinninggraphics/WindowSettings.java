package dinninggraphics;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class WindowSettings extends Frame {

	protected static final int HEIGHT = 200;
	protected static final int WIDTH = 200;
	private static final long serialVersionUID = 1L;
	
	public WindowSettings() {
		this.setTitle("Dining Philosophers");
		this.setSize(WIDTH, HEIGHT);
		this.setBackground(Color.darkGray);
		this.setResizable(false);
		this.setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				System.exit(1);
			}
		});
	}
}
