package dinningphilosopher;

import dinningservices.DiningSetup;

public class DiningPhilosophers {

	public static void main(String args[]) {
		DiningSetup game = new DiningSetup();
		game.startGame();
	}
}
