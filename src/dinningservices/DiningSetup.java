package dinningservices;

import java.util.ArrayList;
import java.util.List;

import dinningcomponents.Chopstick;
import dinningcomponents.Philosopher;
import dinninggraphics.GraphicTable;

public class DiningSetup {

	private static List<Chopstick> chopsticks;
	private static List<Philosopher> philosophers;
	private static final GraphicTable TABLE = new GraphicTable();
	private static final int NUMBER_OF_PHILOSOPHER = 5;
	
	public DiningSetup() {
		chopsticks = new ArrayList<>();
		philosophers = new ArrayList<>();
		createData();
	}

	public void startGame() {
		for (Philosopher philosopher : philosophers) {
			philosopher.start();
		}
	}

	private static void createData() {
		createChopsticks();
		createPhilosophers();
	}

	private static void createPhilosophers() {
		for (int position = 0; position < NUMBER_OF_PHILOSOPHER; position++) {
			int nextChopstick = position == 0 ? NUMBER_OF_PHILOSOPHER - 1 : position - 1;
			
			addPhilosopher(position, nextChopstick);
		}
	}

	private static void addPhilosopher(int position, int nextChopstick) {
		if (isTheLastPhilosopher(position)) {
			philosophers.add(new Philosopher(position, TABLE, chopsticks.get(nextChopstick), chopsticks.get(position)));
		} else {
			philosophers.add(new Philosopher(position, TABLE, chopsticks.get(position), chopsticks.get(nextChopstick)));				
		}
	}

	private static boolean isTheLastPhilosopher(int i) {
		return i == NUMBER_OF_PHILOSOPHER - 1;
	}

	private static void createChopsticks() {
		for (int i = 0; i < NUMBER_OF_PHILOSOPHER; i++) {
			chopsticks.add(new Chopstick(i));
		}
	}

	public List<Chopstick> getChopsticks() {
		return chopsticks;
	}

	public List<Philosopher> getPhilosophers() {
		return philosophers;
	}

	public GraphicTable getTable() {
		return TABLE;
	}
	
	

}
